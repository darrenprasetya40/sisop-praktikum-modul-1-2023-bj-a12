echo "=== REGISTER HERE ==="

echo "Input new username: "
read username

# q untuk menghilangkan print dari grep, o untuk hanya cocokkan dengan pola pencarian, dan w untuk hanya cocokkan kata utuh/lengkap
if grep -qow $username ./user/user.txt 
then
  echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
  echo "Failed, users already exists! Enter another username"
  exit 1
else
  echo $username >> user/user.txt
fi

echo "Input new password: "
read -s password
if wc -c $password -lt 8
  then
    echo "Failed, Password must be at least 8 characters"
elif !echo $password | grep -q '[A-Z]' || echo $password | grep -q '[a-z]'
  then
    echo "Password must contain at least 1 uppercase letter and 1 lowercase letter"
fi

